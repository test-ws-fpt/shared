package error

import "fmt"

type ResultError struct {
	ErrorCode int
	Err error
}

func (r *ResultError) Error() string {
	return fmt.Sprintf("status %d: err %v", r.ErrorCode, r.Err)
}
