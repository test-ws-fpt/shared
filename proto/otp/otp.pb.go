// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.21.0
// 	protoc        v3.11.4
// source: proto/otp/otp.proto

package go_micro_service_otp

import (
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type RequestEmailOtpRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Email *string `protobuf:"bytes,1,req,name=email" json:"email,omitempty"`
}

func (x *RequestEmailOtpRequest) Reset() {
	*x = RequestEmailOtpRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_otp_otp_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RequestEmailOtpRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RequestEmailOtpRequest) ProtoMessage() {}

func (x *RequestEmailOtpRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_otp_otp_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RequestEmailOtpRequest.ProtoReflect.Descriptor instead.
func (*RequestEmailOtpRequest) Descriptor() ([]byte, []int) {
	return file_proto_otp_otp_proto_rawDescGZIP(), []int{0}
}

func (x *RequestEmailOtpRequest) GetEmail() string {
	if x != nil && x.Email != nil {
		return *x.Email
	}
	return ""
}

type RequestEmailOtpResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Success *bool `protobuf:"varint,1,req,name=success" json:"success,omitempty"`
}

func (x *RequestEmailOtpResponse) Reset() {
	*x = RequestEmailOtpResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_otp_otp_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RequestEmailOtpResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RequestEmailOtpResponse) ProtoMessage() {}

func (x *RequestEmailOtpResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_otp_otp_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RequestEmailOtpResponse.ProtoReflect.Descriptor instead.
func (*RequestEmailOtpResponse) Descriptor() ([]byte, []int) {
	return file_proto_otp_otp_proto_rawDescGZIP(), []int{1}
}

func (x *RequestEmailOtpResponse) GetSuccess() bool {
	if x != nil && x.Success != nil {
		return *x.Success
	}
	return false
}

type ConfirmEmailOtpRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Email *string `protobuf:"bytes,1,req,name=email" json:"email,omitempty"`
	Code  *string `protobuf:"bytes,2,req,name=code" json:"code,omitempty"`
}

func (x *ConfirmEmailOtpRequest) Reset() {
	*x = ConfirmEmailOtpRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_otp_otp_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ConfirmEmailOtpRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ConfirmEmailOtpRequest) ProtoMessage() {}

func (x *ConfirmEmailOtpRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_otp_otp_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ConfirmEmailOtpRequest.ProtoReflect.Descriptor instead.
func (*ConfirmEmailOtpRequest) Descriptor() ([]byte, []int) {
	return file_proto_otp_otp_proto_rawDescGZIP(), []int{2}
}

func (x *ConfirmEmailOtpRequest) GetEmail() string {
	if x != nil && x.Email != nil {
		return *x.Email
	}
	return ""
}

func (x *ConfirmEmailOtpRequest) GetCode() string {
	if x != nil && x.Code != nil {
		return *x.Code
	}
	return ""
}

type ConfirmEmailOtpResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Success *bool `protobuf:"varint,1,req,name=success" json:"success,omitempty"`
}

func (x *ConfirmEmailOtpResponse) Reset() {
	*x = ConfirmEmailOtpResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_otp_otp_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ConfirmEmailOtpResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ConfirmEmailOtpResponse) ProtoMessage() {}

func (x *ConfirmEmailOtpResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_otp_otp_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ConfirmEmailOtpResponse.ProtoReflect.Descriptor instead.
func (*ConfirmEmailOtpResponse) Descriptor() ([]byte, []int) {
	return file_proto_otp_otp_proto_rawDescGZIP(), []int{3}
}

func (x *ConfirmEmailOtpResponse) GetSuccess() bool {
	if x != nil && x.Success != nil {
		return *x.Success
	}
	return false
}

var File_proto_otp_otp_proto protoreflect.FileDescriptor

var file_proto_otp_otp_proto_rawDesc = []byte{
	0x0a, 0x13, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6f, 0x74, 0x70, 0x2f, 0x6f, 0x74, 0x70, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x14, 0x67, 0x6f, 0x2e, 0x6d, 0x69, 0x63, 0x72, 0x6f, 0x2e,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x6f, 0x74, 0x70, 0x1a, 0x1c, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x2e, 0x0a, 0x16, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x01, 0x20, 0x02,
	0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x22, 0x33, 0x0a, 0x17, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x18,
	0x01, 0x20, 0x02, 0x28, 0x08, 0x52, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x22, 0x42,
	0x0a, 0x16, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x72, 0x6d, 0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74,
	0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69,
	0x6c, 0x18, 0x01, 0x20, 0x02, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x12,
	0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x02, 0x28, 0x09, 0x52, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x22, 0x33, 0x0a, 0x17, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x72, 0x6d, 0x45, 0x6d, 0x61,
	0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x18, 0x0a,
	0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x18, 0x01, 0x20, 0x02, 0x28, 0x08, 0x52, 0x07,
	0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x32, 0x88, 0x02, 0x0a, 0x03, 0x4f, 0x74, 0x70, 0x12,
	0x8e, 0x01, 0x0a, 0x0f, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x45, 0x6d, 0x61, 0x69, 0x6c,
	0x4f, 0x74, 0x70, 0x12, 0x2c, 0x2e, 0x67, 0x6f, 0x2e, 0x6d, 0x69, 0x63, 0x72, 0x6f, 0x2e, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x6f, 0x74, 0x70, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x2d, 0x2e, 0x67, 0x6f, 0x2e, 0x6d, 0x69, 0x63, 0x72, 0x6f, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x6f, 0x74, 0x70, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x1e, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x18, 0x22, 0x16, 0x2f, 0x6f, 0x74, 0x70, 0x2f, 0x72,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x5f, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x5f, 0x6f, 0x74, 0x70,
	0x12, 0x70, 0x0a, 0x0f, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x72, 0x6d, 0x45, 0x6d, 0x61, 0x69, 0x6c,
	0x4f, 0x74, 0x70, 0x12, 0x2c, 0x2e, 0x67, 0x6f, 0x2e, 0x6d, 0x69, 0x63, 0x72, 0x6f, 0x2e, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x6f, 0x74, 0x70, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69,
	0x72, 0x6d, 0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x2d, 0x2e, 0x67, 0x6f, 0x2e, 0x6d, 0x69, 0x63, 0x72, 0x6f, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x6f, 0x74, 0x70, 0x2e, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x72, 0x6d,
	0x45, 0x6d, 0x61, 0x69, 0x6c, 0x4f, 0x74, 0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00,
}

var (
	file_proto_otp_otp_proto_rawDescOnce sync.Once
	file_proto_otp_otp_proto_rawDescData = file_proto_otp_otp_proto_rawDesc
)

func file_proto_otp_otp_proto_rawDescGZIP() []byte {
	file_proto_otp_otp_proto_rawDescOnce.Do(func() {
		file_proto_otp_otp_proto_rawDescData = protoimpl.X.CompressGZIP(file_proto_otp_otp_proto_rawDescData)
	})
	return file_proto_otp_otp_proto_rawDescData
}

var file_proto_otp_otp_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_proto_otp_otp_proto_goTypes = []interface{}{
	(*RequestEmailOtpRequest)(nil),  // 0: go.micro.service.otp.RequestEmailOtpRequest
	(*RequestEmailOtpResponse)(nil), // 1: go.micro.service.otp.RequestEmailOtpResponse
	(*ConfirmEmailOtpRequest)(nil),  // 2: go.micro.service.otp.ConfirmEmailOtpRequest
	(*ConfirmEmailOtpResponse)(nil), // 3: go.micro.service.otp.ConfirmEmailOtpResponse
}
var file_proto_otp_otp_proto_depIdxs = []int32{
	0, // 0: go.micro.service.otp.Otp.RequestEmailOtp:input_type -> go.micro.service.otp.RequestEmailOtpRequest
	2, // 1: go.micro.service.otp.Otp.ConfirmEmailOtp:input_type -> go.micro.service.otp.ConfirmEmailOtpRequest
	1, // 2: go.micro.service.otp.Otp.RequestEmailOtp:output_type -> go.micro.service.otp.RequestEmailOtpResponse
	3, // 3: go.micro.service.otp.Otp.ConfirmEmailOtp:output_type -> go.micro.service.otp.ConfirmEmailOtpResponse
	2, // [2:4] is the sub-list for method output_type
	0, // [0:2] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_proto_otp_otp_proto_init() }
func file_proto_otp_otp_proto_init() {
	if File_proto_otp_otp_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_proto_otp_otp_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RequestEmailOtpRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_otp_otp_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RequestEmailOtpResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_otp_otp_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ConfirmEmailOtpRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_otp_otp_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ConfirmEmailOtpResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_proto_otp_otp_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_proto_otp_otp_proto_goTypes,
		DependencyIndexes: file_proto_otp_otp_proto_depIdxs,
		MessageInfos:      file_proto_otp_otp_proto_msgTypes,
	}.Build()
	File_proto_otp_otp_proto = out.File
	file_proto_otp_otp_proto_rawDesc = nil
	file_proto_otp_otp_proto_goTypes = nil
	file_proto_otp_otp_proto_depIdxs = nil
}
