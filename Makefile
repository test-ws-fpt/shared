
GOPATH:=$(shell go env GOPATH)
MODIFY=Mproto/imports/api.proto=github.com/micro/go-micro/v2/api/proto

.PHONY: proto
proto:
    
	protoc --proto_path=./ --proto_path=$(GOPATH)/include --micro_out=${MODIFY}:. --go_out=${MODIFY}:. proto/auth/auth.proto
	protoc --proto_path=./ --proto_path=$(GOPATH)/include --micro_out=${MODIFY}:. --go_out=${MODIFY}:. proto/otp/otp.proto
	protoc --proto_path=./ --proto_path=$(GOPATH)/include --micro_out=${MODIFY}:. --go_out=${MODIFY}:. proto/event/event.proto
	protoc --proto_path=./ --proto_path=$(GOPATH)/include --micro_out=${MODIFY}:. --go_out=${MODIFY}:. proto/application/application.proto

