module ems/shared

go 1.14

require (
	github.com/golang/protobuf v1.4.0
	github.com/micro/go-micro/v2 v2.9.1
	github.com/streadway/amqp v1.0.0
	google.golang.org/genproto v0.0.0-20191216164720-4f79533eabd1
	google.golang.org/protobuf v1.22.0
)
